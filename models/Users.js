const mongoose = require('mongoose');
const Joi = require('joi');

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const validateUser = (user) => {
  const schema = Joi.object({
    email: Joi.string().min(3).required().email(),
    password: Joi.string().min(4).max(20).required(),
    role: Joi.string().valid('DRIVER', 'SHIPPER'),
  });

  return schema.validate(user);
};

const validateLogin = (user) => {
  const schema = Joi.object({
    email: Joi.string().min(3).required().email(),
    password: Joi.string().min(4).max(20).required(),
  });

  return schema.validate(user);
};

const validatePassword = (password) => {
  const schema = Joi.object({
    oldPassword: Joi.string().min(4).max(20).required(),
    newPassword: Joi.string().min(4).max(20).required(),
  });

  return schema.validate(password);
};

const validateEmail = (email) => {
  const schema = Joi.object({
    email: Joi.string().min(3).required().email(),
  });

  return schema.validate(email);
};

module.exports = {
  User,
  validateUser,
  validateLogin,
  validatePassword,
  validateEmail,
};
