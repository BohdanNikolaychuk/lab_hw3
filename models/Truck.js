const mongoose = require('mongoose');
const Joi = require('joi');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRIGHT', 'LARGE STRIGHT'],
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
  },
  payload: {
    type: Number,
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const Truck = mongoose.model('Truck', truckSchema);

const validateTruck = (truck) => {
  const schema = Joi.object({
    type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  });

  return schema.validate(truck);
};

module.exports = { Truck, validateTruck };
