const express = require('express');

const router = express.Router();


const { getUser, changePassword, deleteUser } = require('../controllers/user');

router.get('/', getUser);
router.delete('/', deleteUser);
router.patch('/password', changePassword);

module.exports = {
  userRouter: router,
};
