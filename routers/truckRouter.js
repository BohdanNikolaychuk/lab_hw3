const express = require('express');

const router = express.Router();
const {
  createTruck,
  getTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/trucks');


router.post('/', createTruck);
router.get('/', getTrucks);

router.get('/:id', getTruckById);
router.put('/:id', updateTruck);
router.delete('/:id', deleteTruck);

router.post('/:id/assign', assignTruck);

module.exports = {
  truckRouter: router,
};
