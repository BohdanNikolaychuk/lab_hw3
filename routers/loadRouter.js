const express = require('express');

const router = express.Router();

const {
  getLoads,
  createLoad,
  getActiveLoadForDriver,
  changeLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getShippingInfo,
} = require('../controllers/loads');

router.get('/', getLoads);
router.post('/', createLoad);
router.get('/active', getActiveLoadForDriver);
router.patch('/active/state', changeLoadState);
router.get('/:id', getLoadById);
router.put('/:id', updateLoadById);
router.delete('/:id', deleteLoadById);
router.post('/:id/post', postLoadById);
router.get('/:id/shipping_info', getShippingInfo);

module.exports = {
  loadRouter: router,
};
