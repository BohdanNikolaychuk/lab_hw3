const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');


const PORT = 8080;
require('dotenv').config();
// db connect
mongoose.connect(process.env.DB);

// routers
const { authRouter } = require('./routers/authRouter');
const { userRouter } = require('./routers/userRouter');
const { truckRouter } = require('./routers/truckRouter');
const { loadRouter } = require('./routers/loadRouter');


const { authMiddleware } = require('./middleware/authMiddleware');


app.use(express.json());
app.use(morgan('tiny'));


app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);


const start = async () => {
  try {
    app.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(errorHandler);

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: err.massage });
}


