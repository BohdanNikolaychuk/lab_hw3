const jwt = require('jsonwebtoken');

const authMiddleware = async (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    res.status(400).json({
      message: 'Please, provide authorization header',
    });
  }


  const [, token] = authorization.split(' ');

  if (!token) {
    res.status(400).json({
      message: 'Please, include token to request',
    });
  }
  try {
    const decoded = jwt.verify(token, process.env.secret_jwt_key);
    const { _id, role, email, created_date } = decoded;
    req.user = {
      _id,
      role,
      email,
      created_date,
    };
    next();
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};

module.exports = {
  authMiddleware,
};

