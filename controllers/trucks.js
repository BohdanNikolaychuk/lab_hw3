const { Truck, validateTruck } = require('../models/Truck');

const getTrucks = async (req, res) => {
  const { _id, role } = req.user;
  if (role === 'SHIPPER') {
    return res.status(400).json({
      message: 'Error',
    });
  }

  const trucks = await Truck.find({ created_by: _id }).select(['-__v', '-dimensions', '-payload']);
  res.status(200).json({ trucks });
};

const createTruck = async (req, res) => {
  const { error } = validateTruck(req.body);
  if (error) {
    return res.status(400).json({
      message: 'Error',
    });
  }

  const { _id, role } = req.user;
  if (role === 'SHIPPER') {
    return res.status(400).json({
      message: 'Only Drivers can add trucks.',
    });
  }

  const { type } = req.body;
  let payload;
  let dimensions = {};

  if (type === 'SPRINTER') {
    payload = 1700;
    dimensions = {
      width: 300,
      length: 250,
      height: 170,
    };
  } else if (type === 'SMALL STRAIGHT') {
    payload = 2500;
    dimensions = {
      width: 500,
      length: 250,
      height: 170,
    };
  } else if (type === 'LARGE STRAIGHT') {
    payload = 4000;
    dimensions = {
      width: 700,
      length: 350,
      height: 200,
    };
  }

  const truck = new Truck({
    created_by: _id,
    type,
    payload,
    dimensions,
  });
  await truck.save();

  res.status(200).json({ message: 'Truck created successfully' });
};

const getTruckById = async (req, res) => {
  const { _id, role } = req.user;
  if (role === 'SHIPPER') {
    return res.status(400).json({
      message: 'Only Drivers can add trucks.',
    });
  }

  const { id } = req.params;
  const truck = await Truck.findOne({ created_by: _id, _id: id }).select([
    '-__v',
    '-dimensions',
    '-payload',
  ]);
  if (!truck) {
    return res.status(400).json({
      message: `Truck with ID ${id} does not exist.`,
    });
  }

  res.status(200).json({ truck });
};

const updateTruck = async (req, res) => {
  const { error } = validateTruck(req.body);
  if (error) {
    return res.status(400).json({
      message: 'Error',
    });
  }

  const { _id, role } = req.user;
  if (role === 'SHIPPER') {
    return res.status(400).json({
      message: 'Only Drivers can update trucks.',
    });
  }

  // find and validate the truck
  const { id } = req.params;
  const truck = await Truck.findOne({ created_by: _id, _id: id });

  if (!truck) {
    return res.status(400).json({
      message: `Truck with ID ${id} does not exist.`,
    });
  }
  if (!truck.status === 'OL') {
    return res.status(400).json({
      message: 'You can not update trucks On Load.',
    });
  }
  if (!truck.created_by === !truck.assigned_to) {
    return res.status(400).json({
      message: 'You can only update trucks that are not assigned to you.',
    });
  }

  const { type } = req.body;
  await Truck.findOneAndUpdate({ created_by: _id, _id: id }, { type });
  res.status(200).json({ message: 'Truck details changed successfully' });
};

const deleteTruck = async (req, res) => {
  // get required IDs and validate user role
  const { _id, role } = req.user;
  if (role === 'SHIPPER') {
    return res.status(400).json({
      message: 'Only Drivers can delete trucks.',
    });
  }

  // find and validate the truck
  const { id } = req.params;
  const truck = await Truck.findOne({ created_by: _id, _id: id });

  if (!truck) {
    return res.status(400).json({
      message: `Truck with ID ${id} does not exist.`,
    });
  }
  if (!truck.status === 'OL') {
    return res.status(400).json({
      message: 'You can not delete trucks On Load.',
    });
  }
  if (!truck.created_by === !truck.assigned_to) {
    return res.status(400).json({
      message: 'You can only delete trucks that are not assigned to you.',
    });
  }

  await Truck.findOneAndDelete({ created_by: _id, _id: id });
  res.status(200).json({ message: 'Truck deleted successfully' });
};

const assignTruck = async (req, res) => {
  // get required IDs and validate user role
  const { _id, role } = req.user;
  if (role === 'SHIPPER') {
    return res.status(400).json({
      message: 'Only Drivers can assign trucks.',
    });
  }

  // check if driver has a truck
  const assignedTruck = await Truck.findOne({ assigned_to: _id });
  if (assignedTruck) {
    return res.status(400).json({
      message: 'You have already been assigned to another truck.',
    });
  }

  // find and validate the truck
  const { id } = req.params;
  const truck = await Truck.findOne({ created_by: _id, _id: id });
  if (!truck) {
    return res.status(400).json({
      message: `Truck with ID ${id} does not exist.`,
    });
  }
  if (!truck.status === 'OL') {
    return res.status(400).json({
      message: 'You can not assign trucks On Load.',
    });
  }
  if (!truck.created_by === !truck.assigned_to) {
    return res.status(400).json({
      message: 'You can only assign trucks that are not assigned to you..',
    });
  }

  await Truck.findOneAndUpdate({ created_by: _id, _id: id }, { assigned_to: _id });

  res.status(200).json({ message: 'Truck assigned successfully' });
};

module.exports = {
  createTruck,
  getTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
};
