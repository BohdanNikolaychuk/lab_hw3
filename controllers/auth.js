const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { User, validateUser, validateLogin } = require('../models/Users');

const registerUser = async (req, res) => {
  const { error } = validateUser(req.body);
  if (error) {
    res.status(400).json({
      message: 'The field must not be empty',
    });
  }
  const { email, password, role } = req.body;


  if (!email || !password || !role) {
    res.status(400).json({ message: 'The field must not be empty' });
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  return await res.status(200).json({
    message: 'Profile created successfully',
  });

};

const loginUser = async (req, res) => {
  const { error } = validateLogin(req.body);
  if (error) {
    res.status(400).json({
      message: 'Missing required email and password fields',
    });
  }
  const user = await User.findOne({ email: req.body.email });


  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const jwtToken = jwt.sign(
      {
        _id: user._id,
        email: user.email,
        role: user.role,
        created_date: user.created_date,
      },
      process.env.secret_jwt_key,
    );
    return res.status(200).json({ jwt_token: jwtToken });
  }

  return res.status(403).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};

