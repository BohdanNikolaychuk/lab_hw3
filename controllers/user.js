const bcrypt = require('bcryptjs');
const { User } = require('../models/Users');
const { Truck } = require('../models/Truck');

const getUser = async (req, res) => {
  res.status(200).json({ user: req.user });
};

const changePassword = async (req, res) => {
  // const { error } = validatePassword(req.body);
  // if (error) {
  //   return res.status(400).json({
  //     message: 'Error',
  //   });
  // }

  const { _id, role } = req.user;
  const user = await User.findOne({ _id });

  if (role === 'DRIVER') {
    const truck = await Truck.findOne({ assigned_to: _id, status: 'OL' });
    if (truck) {
      return res.status(400).json({
        message: 'You cannot change your password while you have On Loaded Truck.',
      });
    }
  }

  const { oldPassword, newPassword } = req.body;

  if (
    (await bcrypt.compare(oldPassword, user.password))
    && !(await bcrypt.compare(newPassword, user.password))
  ) {
    await User.findOneAndUpdate({ _id }, { password: await bcrypt.hash(newPassword, 10) });
  } else {
    return res.status(400).json({
      message: 'Please provide a valid password',
    });
  }

  res.status(200).json({ message: 'Success! Password has been changed.' });
};

const deleteUser = async (req, res) => {
  const { _id, role } = req.user;

  if (role === 'DRIVER') {
    const truck = await Truck.findOne({ assigned_to: _id, status: 'OL' });
    if (truck) {
      return res.status(400).json({
        message: 'You cannot delete your profile while you have On Loaded Truck.',
      });
    }
  }
  await User.findByIdAndRemove({ _id: req.user._id });
  res.status(200).json({ message: 'Profile deleted successfully' });
};

module.exports = { getUser, changePassword, deleteUser };
